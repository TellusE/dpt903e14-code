;; Tries to create a sync point.

(def MAX_REINDEER 9)
(def MAX_ELVES 7)
(def MAX_WAITING_ELVES 3)

;; The elfQueue is simply a queue of promises, populated by an Elf when he has
;; a problem. The Elf blocks, waiting for santa to fulfil the promise (hehehe).
(def elfQueue
  (ref () :validator list?))

;; Enqueues a promise at the back of the queue.
(defn queue-problem []
  (let [prom (promise)]
    (dosync
     (ref-set elfQueue (reverse (conj (reverse @elfQueue) prom))))
    prom))

;; Pops a promise from the front of the queue and returns it.
(defn pop-problem []
  (let [pp (first @elfQueue)]
    (dosync
     (ref-set elfQueue (rest @elfQueue)))
    pp))

(def sleighPromise (ref (promise)))
(def waitingReindeer (ref 0 :validator (fn [v]
                                         (and
                                          (integer? v)
                                          (<= 0 v)
                                          (<= v MAX_REINDEER)))))
(def reindeerReady (ref false))

(defn alert-santa []
  (dosync
   (ref-set reindeerReady true)))

(defn spark [f]
  (.start
   (Thread. f)))

(defn seconds [ms]
  (* ms 1000))

(defn reindeer []
  (while true
    (Thread/sleep (rand-int (seconds 10)))
    (let [rCount (dosync (ref-set waitingReindeer (inc @waitingReindeer)))]
      (cond
       (< rCount (dec MAX_REINDEER)) @sleighPromise
       (= rCount (dec MAX_REINDEER)) (alert-santa)))))

(defn elf []
  (while true
    (Thread/sleep (seconds 1))
    (if (zero? (rand-int 1)) ;; Randomly determine if the elf has a problem.
      (let [prom (promise)] ;; Enqueue and block on promise if problem.
        (println "Elf: HAVE PROBLEM!")
        (println "Elf: santa delivers " @(queue-problem))))))

(defn deliver-presents []
  (dosync
   (println "Sleigh time!")
   (deliver sleighPromise "Mush!")
   (ref-set waitingReindeer 0)
   (ref-set sleighPromise (promise)) ;; Reset promise?
   (ref-set reindeerReady false))) ;; Stop further deliveries.

;; Takes a promise and blocks until it is delivered.
(defn santa []
  (while true
    (cond
     @reindeerReady (deliver-presents)
     (>= (count @elfQueue) MAX_WAITING_ELVES) ((fn []
                                                 (println "Fixing elf problem.")
                                                 (deliver (pop-problem) "Fixed!"))))))

(defn recursiveSpark [f, c]
  (spark f)
  (if (> c 0)
    (recursiveSpark f (dec c))))

(recursiveSpark elf MAX_ELVES)
(recursiveSpark reindeer MAX_REINDEER)
(spark santa)
