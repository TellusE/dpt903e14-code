(defn afun []
  (let [myfun #(println "A:" (first %) ", B:" (second %))
        myfun2 #(> 0 (count %))
        some_list (list 5 1 6 21 "short")]
    (myfun some_list)
    (myfun2 some_list)))

(afun)
