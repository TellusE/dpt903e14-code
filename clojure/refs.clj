; Ref fun fun file.

(def myRef
  (ref 0 :validator number?))

(defn safeIterate [r] (dosync (ref-set r (+ @r 1))))

(dosync
 (ref-set myRef 3))

(println "Hi! Having fun.")
(println "Current value:" @myRef)
(println "Changed to" (safeIterate myRef))
