; Define Fibonacci recursively. 
(def fibonacci (fn [n]
                 (if (< n 2)
                   n
                   (+ (fibonacci (- n 1))
                      (fibonacci (- n 2))))))
; Read, and validate, command line arguments.
(if
    (and (= (count *command-line-args*)) (number? *command-line-args*) 0)
  (print "Run with: clojure fibonacci.clj NUM\n")
  (print
   (format "fib(%s): %s\n"
           (Integer. (first *command-line-args*))
           (fibonacci (Integer. (first *command-line-args*))) "\n")))
