(defn quicksort [aVector]
  (if (< (count aVector) 2) aVector
      (let [pivot (get aVector (int (/ (count aVector) 2)))
            left (filter #(< % pivot) aVector)
            center (filter #(= % pivot) aVector)
            right (filter #(> % pivot) aVector)]
        (lazy-cat (quicksort left) center (quicksort right)))))

(defn random-vector
  ([size] (random-vector size 10000))
  ([size seed]
   (let [r (java.util.Random. seed)]
     (repeatedly size #(.nextInt r 1000)))))

(defn profile-single
  ([size seed] (profile-single (random-vector size seed)))
  ([a-vector]
   (let [start-time (System/nanoTime)]
     (do
       (quicksort a-vector)
       (/ (float (- (System/nanoTime) start-time)) 1000000)))))

(defn profile [size run-count]
  (let [v (random-vector size)]
    (repeatedly run-count #(profile-single (random-vector size)))))

(defn sum [coll]
  (let [sum (fn [a b] (+ a b))]
    (reduce sum 0 coll)))

(defn avg [coll]
  (/ (sum coll) (count coll)))

(defn print-run
  ([size run-count] (print-run size run-count true))
  ([size run-count show-result]
   (let [result (profile size run-count)]
     (do
       (println size "/" run-count)
       (if show-result (println result))
       (println "Min:" (apply min result) "Max:" (apply max result) "Avg:" (avg result))))))

(do
  (println "Sequential quicksort (all times in ms):")
  (print-run 5000 100 false)
  (print-run 50000 100 false)
  (print-run 500000 100 false)
  (print-run 5000000 100 false))
