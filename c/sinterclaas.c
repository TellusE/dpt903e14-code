#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <string.h>

/* use P and V for wait and signal semaphore operations. */

#define REINDEER 9 // Max number of reindeer.
#define ELVES 3 // Size of an elf group for Santa to wake up.

sem_t only_elves,
  emutex,
  rmutex,
  rein_wait,
  sleigh,
  done,
  santa_signal,
  santa,
  problem,
  elf_done;

#define SEMINIT(s,v) sem_init(&s, 0, v)

// Initializes semaphores
void init_sems(void) {
  SEMINIT(only_elves, 3);
  SEMINIT(emutex, 1);
  SEMINIT(rmutex, 1);
  SEMINIT(rein_wait, 0);
  SEMINIT(sleigh, 0);
  SEMINIT(done, 0);
  SEMINIT(santa_signal, 0);
  SEMINIT(santa, 0);
  SEMINIT(problem, 0);
  SEMINIT(elf_done, 0);
}

int rein_ct = 0,
  elf_ct = 0;

// Main thread handles user input for querying and stopping threads.
pthread_t rthread, // Reindeer thread.
  ethread, // Elf thread
  sthread; // Santa thread.

// Signal values. Simple integer signalling used.
int rsignal = 0, esignal = 0, ssignal = 0;

int i; // Global iterator value.

void* rfn(void* ptr) {
  // tan on beaches until Christmas is close.
  // TODO: Implement signal handling with main thread.
  for(;;) {
    sem_wait(&rmutex);
    rein_ct++;
    if (rein_ct == REINDEER) {
      sem_post(&rmutex);
      sem_post(&santa);
    } else {
      sem_wait(&rmutex);
      sem_post(&rein_wait);
    }
  }
}

void* efn(void* ptr) {
  // TODO: Implement signal handling with main thread.
  for(;;) {
    sem_wait(&only_elves);
    sem_wait(&emutex);
    elf_ct++;
    if (elf_ct == ELVES) {
      sem_post(&emutex);
      sem_post(&santa); // 3rd elf awakens Santa.
    } else {
      sem_wait(&emutex);
      sem_post(&santa_signal); // Wait outside shop.
    }
    sem_wait(&problem);
    // Ask a question.
    // TODO: Implement?
    sem_wait(&elf_done);
    sem_post(&only_elves);
  }
}

void* sfn(void* ptr) {
  for(;;) {
    sem_wait(&santa);
    if (rein_ct == REINDEER) {
      rein_ct = 0;
      for (i = 0; i < REINDEER - 1; i++)
        sem_post(&rein_wait);
      for (i = 0; i < REINDEER; i++)
        sem_post(&sleigh);
      // Deliver toys and return.
      for (i = 0; i < REINDEER; i++)
        sem_post(&done);
    } else { // Elves arrived.
      for (i = 0; i < ELVES - 1; i++)
        sem_post(&santa_signal);
      sem_wait(&emutex);
      elf_ct = 0;
      sem_post(&emutex);
      for (i = 0; i < ELVES; i++) {
        sem_post(&problem);
        // Answer the question.
        sem_post(&elf_done);
      }
    }
  }
}

void cancel_threads() {
  pthread_cancel(rthread);
  pthread_cancel(ethread);
  pthread_cancel(sthread);
}

int main(int argc, char* argv[]){
  printf("Santa Claus reference implementation.\n");

  init_sems();
  
  printf("Starting reindeer thread...");
  pthread_create(&rthread, NULL, &rfn, (void*) &rsignal);
  printf("DONE\n");
  printf("Starting elf thread...");
  pthread_create(&ethread, NULL, &efn, (void*) &esignal);
  printf("DONE\n");
  printf("Starting Santa thread...");
  pthread_create(&sthread, NULL, &sfn, (void*) &ssignal);
  printf("DONE\n");

  printf("System running.\n");

  int done = 0;
  char cmd[20];

  while (!done) {
    printf("CMD|> ");
    scanf("%s", &cmd);
    if (strcmp(cmd, "quit") == 0) {
      printf("Exit signal received. Ending.\n");
      done = 1;
      cancel_threads();
    } else if (strcmp(cmd, "help") == 0) {
      printf("Known commands: quit\n");
    } else {
      printf("Unknown command '%s'. Try 'help'.\n", &cmd);
    }
  }
  
  printf("Bye!\n");
  
  return 0;
}
