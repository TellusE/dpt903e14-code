-module(santa).
-export([speak/1, barn/1, reindeer/2]).

% Santa module. Function is simple:
% Reindeer check with the barn. Either it's full (then go to Santa) or its roomy (rest).
% Elves get a problem, go to the door. The door notifies Santa if three elves arrive.
% Santa takes visitors, solves problems.
% The barn handles capacity and registered reindeer.

evictReindeer(Target) ->
    Target ! evict.

stable(Capacity) ->
    stable(Capacity, []).
stable(Capacity, Occupants) ->
    receive % Barn handles one type of request. Lodge. Returns a rejection if full.
        {lodge, Reindeer} ->
            if
                length(Occupants) == Capacity ->
                    Reindeer ! disallowStable,
                    stable(Capacity, Occupants);
                true ->
                    Reindeer ! allowStable,
                    stable(Capacity, Occupants ++ [Reindeer])
            end;
        deliverPresents ->
            foreach(fun(Reindeer) -> Reindeer ! evict end, Occupants),
            stable(Capacity)
    end.

reindeer() ->
    % Message the Barn to see if its full. If not, rest. If it is, go to Santa.
    stablePid ! {lodge, self()},
    receive
        disallowStable ->
            santaPid ! {wakeUp, self()},
            receive
                deliverPresents ->
                    reindeer()
            end;
        allowStable ->
            receive
                deliverPresents ->
                    reindeer()
            end
    end.

santa() ->
    receive
        reindeerReady ->
            stablePid ! deliverPresents,
            santa();
        elvesWithProblem ->
            queuePid ! problemFixed,
            santa()
    end.

elf() ->
    queuePid ! { newProblem, self() },
    receive
        problemFixed ->
            elf()
    end.

problemQueue() ->
    problemQueue(3).
problemQueue(Threshold) ->
    problemQueue(Threshold, 0).
problemQueue(Threshold, Queued) ->
    if
        Threshold >= length(Queued) ->
            santaPid ! elvesWithProblem,
            receive
                problemFixed ->
                    foreach(fun(Elf) -> Elf ! problemFixed end, lists:sublist(Queued, 3)),
                    problemQueue(Threshold, lists:sublist(Queued,Threshold,length(Queued)-Threshold))
            end;
        true ->
            receive
                { newProblem, Elf } ->
                    problemQueue(Threshold, Queued ++ [Elf])
            end
    end
end.

recursiveSpawnAux(func, args, count, lst) ->
    if
        count == 0 ->
            lst
        true ->
            recursiveSpawnAux(func, args, count-1, lst ++ [spawn(santa, reindeer, [])]);
    end

recursiveSpawn(func, args, count) ->
    recursiveSpawnAux(func, args, count, [])

start() ->
    register(queuePid, spawn(santa, problemQueue, [])),
    register(stablePid, spawn(santa, stable, [9])),
    register(santaPid, spawn(santa, santaClaus)),
    Reindeer = recursiveSpawn(reindeer, [], 9),
    Elves = recursiveSpawn(elf, [], 7).
