-module(quicksort_par).
-export([random_list/1,quicksort/1,run_test/0,run_test/1,availableWorkers/0,addJob/2,quicksorter/1,quicksorter/3,generateWorkers/2,workerPool/0]).
-on_load(module_init/0).

random_list(Count) ->
    % Create a bad seed for the list. Save old state so we can restore it.
    OldSeed = random:seed(0, 0, 0),
    RetVal = random_list(Count, []),
    if
        OldSeed /= undefined -> % If run on a fresh VM instance, we can't revert.
            random:seed(OldSeed);
        true ->
            do_nothing
    end,
    RetVal.
random_list(Count, Aggregation) ->
    if
        Count == 0 ->
            Aggregation;
        true ->
            random_list(Count-1, Aggregation ++ [random:uniform(1000)])
    end.

quicksort(SomeList) when length(SomeList) < 2 ->
    SomeList;
quicksort(List) ->
    io:format("Sending job ~w to manager.~n", [List]),
    addJob(List, self()),
    receive { done, Sorted } ->
            Sorted
    end.

run_test() ->
    run_test(5000).
run_test(ElementCount) ->
    {TimeTaken, Result} = timer:tc(quicksort_par, quicksort, [random_list(ElementCount)]),
    io:format("Sorted ~p elements. Took ~pms.~nGarbage: ~w~n", [ElementCount, TimeTaken/1000, Result]).

quicksorter(Master, List) when length(List) < 2 ->
    List;
quicksorter(Master, List) ->
    io:format("[~w]: Sequential non-trivial job ~w.~n", [self(), List]),
    Pivot = lists:nth(random:uniform(length(List)), List),
    Rest = lists:delete(Pivot, List),
    Left = [ L || L <- Rest, L < Pivot ],
    Right = [ R || R <- Rest, R >= Pivot ],
    io:format("[~w]: Pivot: ~w, Rest: ~w, Left: ~w, Right: ~w~n", [self(), Pivot, Rest, Left, Right]),
    AuxJob = addJob(Left, self()),
    io:format("[~w]: Is separate job (left: [~w]) running? ~w~n", [self(), Left, AuxJob]),
    SortedRight = quicksorter(Master, Right), % Always do half ourselves.
    if
        AuxJob == false -> % No extra jobs were possible, do left ourselves.
            SortedLeft = quicksorter(Master, Left),
            io:format("[~w]: Concatenating left(~w), pivot(~w) and right(~w)~n", [SortedLeft, Pivot, SortedRight]),
            Sorted = quicksorter(Master, Left) ++ [Pivot] ++ SortedRight,
            io:format("[~w] Sorting done: ~w~n", [self(), Sorted]),
            Sorted;
        true ->
            receive % Left was parallelized, await result.
                { done, SortedLeft } ->
                    SortedLeft ++ [Pivot] ++ SortedRight
            end
    end.
quicksorter(Master, List, Return) when length(List) < 2 ->
    io:format("[~w]: Received trivial job. Responding ~w.~n", [self(), List]),
    Return ! { done, List }, % Return trivial.
    quicksorter(Master); % Go back to idle mode.
quicksorter(Master, List, Return) ->
    io:format("[~w]: Received non-trivial job ~w.~n", [self(), List]),
    Pivot = lists:nth(random:uniform(length(List)), List),
    Rest = lists:delete(Pivot, List),
    Left = [ L || L <- Rest, L < Pivot ],
    Right = [ R || R <- Rest, R >= Pivot ],
    io:format("[~w]: Pivot: ~w, Rest: ~w, Left: ~w, Right: ~w~n", [self(), Pivot, Rest, Left, Right]),
    AuxJob = addJob(Left, self()),
    io:format("[~w]: Is separate job (left: [~w]) running? ~w~n", [self(), Left, AuxJob]),
    SortedRight = quicksorter(Master, Right), % Always do half ourselves.
    if
        AuxJob == false -> % No extra jobs were possible, do left ourselves.
            SortedLeft = quicksorter(Master, Left),
            io:format("[~w]: Concatenating left(~w), pivot(~w) and right(~w)~n", [SortedLeft, Pivot, SortedRight]),
            Return ! { done, SortedLeft ++ [Pivot] ++ SortedRight };
        true ->
            receive % Left was parallelized, await result.
                { done, SortedLeft } ->
                    Sorted = SortedLeft ++ [Pivot] ++ SortedRight,
                    io:format("[~w]: Responding with ~w~n", [self(), Sorted]),
                    Return ! { done, Sorted },
                    quicksorter(Master) % Back to idle mode.
            end
    end.
quicksorter(Master) ->
    io:format("[~w]: Idle mode.~n", [self()]),
    Master ! { incfree, self() }, % Free up again.
    receive
        { job, List, Return } ->
            Master ! { decfree, self() }, % Occupy free slot.
            quicksorter(Master, List, Return)
    end.

generateWorkers(Count, Master) ->
    generateWorkers(Count, Master, []).
generateWorkers(Count, Master, Aggregator) ->
    if
        Count == 0 ->
            Aggregator;
        true ->
            generateWorkers(Count - 1, Master, Aggregator ++ [spawn(quicksort_par, quicksorter, [Master])])
    end.

% The worker pool spins up a number of quicksort workers and lets them wait for
% a task.
workerPool() ->
    generateWorkers(erlang:system_info(logical_processors_available), self()),
    workerPool([]).
workerPool(Workers) ->
    %io:format("WorkerPool: ~w workers free.~n", [length(Workers)]),
    receive
        { free, Return } ->
            % io:format("Responding ~w to querant ~w.~n", [length(Workers), Return]),
            Return ! { free, length(Workers) },
            workerPool(Workers);
        { addjob, Job, Return } ->
            lists:nth(1, Workers) ! { job, Job, Return },
            workerPool(Workers); % Wait for occupy confirm from worker.
        { incfree, Worker } ->
            io:format("Pool: new worker count, ~w~n", [length(Workers)+1]),
            workerPool(Workers ++ [Worker]);
        { decfree, Worker } ->
            io:format("Pool: new worker count, ~w~n", [length(Workers)-1]),
            workerPool(lists:delete(Worker, Workers))
    end.    
            
availableWorkers() ->
    workerPoolPid ! { free, self() },
    receive
        { free, Count } ->
            Count
    end.

% Adds a new job asynchronously. workerPool will initiate a thread which
% will return to the calling environment when done.
addJob(Job, Return) ->
    AvailableWorkers = availableWorkers(),
    %io:format("AddJob: availility: ~w~n", [AvailableWorkers]),
    if
        AvailableWorkers > 0 -> % Can par? Add job and confirm.
            %io:format("Added new job for ~w~n", [Return]),
            workerPoolPid ! { addjob, Job, Return },
            true;
        true ->
            %io:format("Declined to add new job for ~w", [Return]),
            false % Cannot par? Report so.
    end.

module_init() ->
    io:format("Spawning virtual work pool.~n"),
    WCP = spawn(fun workerPool/0),
    register(workerPoolPid, WCP),
    io:format("All init is now done. Enjoy!~n").
