% Dev file, for quick code tests.
-module(dev).
-export([start/0, myfun/0]).

myfun() ->
    receive
        ping ->
            io:format("PING!~n"),
            myfun();
        pong ->
            io:format("PONG!~n"),
            myfun()
    end.

start() ->
    Pid1 = spawn(dev, myfun, []),
    register(pinger, Pid1).
