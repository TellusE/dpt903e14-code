#! /bin/sh

echo Sequential
echo 5000/$1
./run_quicksort.sh 5000 $1
echo 50000/$1
./run_quicksort.sh 50000 $1
echo 500000/$1
./run_quicksort.sh 500000 $1

echo Parallel
echo 5000/$1
./run_quicksort_par.sh 5000 $1
echo 50000/$1
./run_quicksort_par.sh 50000 $1
echo 500000/$1
./run_quicksort_par.sh 500000 $1
