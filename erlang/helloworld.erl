-module(helloworld).
-export([speak/1]).

speak(Message) ->
    io:format(string:concat(Message, "~n"), []).

barn(Capacity) ->
    self(Capacity, []) % Call with empty list of occupants.
barn(Capacity, Occupants) ->
    receive % Barn only handles two types of requests. Query (IsFull) and Stay.
        {is_full, Reindeer} ->
            Reindeer ! length(Occupants) == Capacity;
        {stay, Reindeer} ->
            Barn(Capacity, Occupants ++ [Reindeer])
    end

reindeer(BarnName, DoorName) ->
    % Message the Barn to see if its full. If not, rest. If it is, go to Santa.
    BarnName ! {IsFull, self()},
    receive
        Full ->
            DoorName ! {WakeSanta, self()};
        NotFull ->
            BarnName ! {Rest, self()}
    end


Elf(DoorNamee) ->
