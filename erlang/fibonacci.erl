-module(fibonacci).
-export([fib/1, parFib/1, parFib/2]).

fib(N) ->
    if
        N < 2 ->
            N;
        true ->
            fib(N - 1) + fib(N - 2)
    end.

% Root call of parallel Fibonacci calculation.
parFib(N) ->
    if
        N < 2 ->
            N;
        true ->
            spawn(fibonacci, parFib, [N - 1, self()]),
            spawn(fibonacci, parFib, [N - 2, self()]),
            receive
                ValA ->
                    receive
                        ValB ->
                            ValA + ValB
                    end
            end
    end.
% Auxillary parFib function. Accumulates spawned results back to the root.
parFib(N, RespondTo) ->
    if
        N < 2 ->
            RespondTo ! N;
        true ->
            spawn(fibonacci, parFib, [N - 1, self()]),
            spawn(fibonacci, parFib, [N - 2, self()]),
            receive
                ValA ->
                    receive ValB ->
                            RespondTo ! (ValA + ValB),
                            (ValA + ValB)
                    end
            end
    end.
