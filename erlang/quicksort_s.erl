-module(quicksort_s).
-export([qsort3/1,run_test/0,run_test/1,profile/2]).

qsort3([]) ->
    [];
qsort3([ H | T ]) ->
    qsort3_acc([ H | T ], []).

qsort3_acc([], Acc) ->
    Acc;
qsort3_acc([ H | T ], Acc) ->
    part_acc(H, T, {[], [H], []}, Acc).

part_acc(_, [], {L, E, G}, Acc) ->
    qsort3_acc(L, (E ++ qsort3_acc(G, Acc)));
part_acc(X, [ H | T ], {L, E, G}, Acc) ->
    if
        H < X ->
            part_acc(X, T, {[ H | L ], E, G}, Acc);
        H > X ->
            part_acc(X, T, {L, E, [ H | G ]}, Acc);
        true ->
            part_acc(X, T, {L, [ H | E ], G}, Acc)
    end.

random_list(Count) ->
    % Create a bad seed for the list. Save old state so we can restore it.
    OldSeed = random:seed(0, 0, 0),
    RetVal = random_list(Count, 1000),
    if
        OldSeed /= undefined -> % If run on a fresh VM instance, we can't revert.
            random:seed(OldSeed);
        true ->
            do_nothing
    end,
    RetVal.
random_list(Count, MaxNum) ->
    [random:uniform(MaxNum) || _ <- lists:seq(1, Count)].

run_test() ->
    run_test(20).
run_test(ElementCount) ->
    io:format("Generating list of ~w elements.~n", [ElementCount]),
    RandomList = random_list(ElementCount),
    io:format("Done.~n"),
    {TimeTaken, Result} = timer:tc(quicksort_par, quicksort, [RandomList]),
    io:format("Sorted ~p elements. Took ~pms.~n", [ElementCount, TimeTaken/1000]),
    if
        ElementCount =< 20 ->
            io:format("Sorted list: ~w~n", [Result]);
        true ->
            io:format("List too long to print. Have fun!~n")
    end.

profile(Size, RunCount) when is_integer(Size) and is_integer(RunCount)->
    Results = [ element(1,timer:tc(quicksort_par, quicksort, [random_list(Size)])) || _ <- lists:seq(1, RunCount)],
    { lists:min(Results), lists:max(Results), lists:foldl(fun(X, Sum) -> X + Sum end, 0, Results)/RunCount };
profile(Size, RunCount) when is_list(Size) ->
    profile(element(1, string:to_integer(Size)), RunCount);
profile(Size, RunCount) when is_list(RunCount) ->
    profile(Size, element(1, string:to_integer(RunCount))).
profile([Size, RunCount]) -> % This one is specifically for cmdline execution.,
    io:format("~p~n", [profile(Size, RunCount)]).
