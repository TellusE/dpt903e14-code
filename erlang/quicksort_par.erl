-module(quicksort_par).
-export([random_list/1,quicksort/1,run_test/0,run_test/1,profile/2,profile/1]).

random_list(Count) ->
    % Create a bad seed for the list. Save old state so we can restore it.
    OldSeed = random:seed(0, 0, 0),
    RetVal = random_list(Count, 1000),
    if
        OldSeed /= undefined -> % If run on a fresh VM instance, we can't revert.
            random:seed(OldSeed);
        true ->
            do_nothing
    end,
    RetVal.
random_list(Count, MaxNum) ->
    [random:uniform(MaxNum) || _ <- lists:seq(1, Count)].

quicksort(List) ->
    quicksort(List, erlang:system_info(logical_processors_available)).
quicksort(List, MaxSpawn) when length(List) < 2 ->
    List;
quicksort(List, MaxSpawn) ->
    Pid = self(),
    % Pivot = lists:nth(random:uniform(length(List)), List),
    Pivot = lists:nth(trunc(length(List)/2), List),
    Rest = lists:delete(Pivot, List),
    Left = [ L || L <- Rest, L < Pivot ],
    Right = [ R || R <- Rest, R >= Pivot],
    if
        MaxSpawn < 2 ->
            quicksort(Left, MaxSpawn) ++ [Pivot] ++ quicksort(Right, MaxSpawn);
        true ->
            spawn(fun() -> Pid ! { leftvalue, quicksort(Left, MaxSpawn - 2) } end),
            spawn(fun() -> Pid ! { rightvalue, quicksort(Right, MaxSpawn - 2) } end),
            receive
                { leftvalue, SortedLeft } ->
                    receive
                        { rightvalue, SortedRight } ->
                            SortedLeft ++ [Pivot] ++ SortedRight
                    end
            end
    end.

run_test() ->
    run_test(20).
run_test(ElementCount) ->
    io:format("Generating list of ~w elements.~n", [ElementCount]),
    RandomList = random_list(ElementCount),
    io:format("Done.~n"),
    {TimeTaken, Result} = timer:tc(quicksort_par, quicksort, [RandomList]),
    io:format("Sorted ~p elements. Took ~pms.~n", [ElementCount, TimeTaken/1000]),
    if
        ElementCount =< 20 ->
            io:format("Sorted list: ~w~n", [Result]);
        true ->
            io:format("List too long to print. Have fun!~n")
    end.

profile(Size, RunCount) when is_integer(Size) and is_integer(RunCount)->
    Results = [ element(1,timer:tc(quicksort_par, quicksort, [random_list(Size)])) || _ <- lists:seq(1, RunCount)],
    { lists:min(Results), lists:max(Results), lists:foldl(fun(X, Sum) -> X + Sum end, 0, Results)/RunCount };
profile(Size, RunCount) when is_list(Size) ->
    profile(element(1, string:to_integer(Size)), RunCount);
profile(Size, RunCount) when is_list(RunCount) ->
    profile(Size, element(1, string:to_integer(RunCount))).
profile([Size, RunCount]) -> % This one is specifically for cmdline execution.,
    io:format("~p~n", [profile(Size, RunCount)]).
