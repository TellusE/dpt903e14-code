import x10.util.Random;

public class Reindeer {

    private val name:String;
    private val santa:GlobalRef[SantaClaus];
    private val random:Random;
    private var gettingHarnessed:Boolean = false;
    private var doneDelivering:Boolean = false;
    private var refThis:GlobalRef[Reindeer];

    public def this(n:String, s:GlobalRef[SantaClaus]) {
        name = n + " @ " + here + ": ";
        santa = s;
        random = new Random();
    }

    public def run() {
        setRef();
        while(true) {
            goOnVacation();
            waitInHut();
            harnessDone();
        }
    }

    public def harness() {
        atomic {
            gettingHarnessed = true;
        }
    }

    public def doneDelivering() {
        atomic {
            doneDelivering = true;
        }
    }

    private def setRef() {
        refThis = GlobalRef[Reindeer](this);
    }

    private def goOnVacation() {
        Console.OUT.println(name + "goes on vacation");
        System.sleep(random.nextLong(20000) + 10000);
    }

    private def waitInHut() {
        Console.OUT.println(name + "arrived at hut");
        at (santa.home) async {
            santa().addToDeerQueue(refThis);
        }
        when(gettingHarnessed) {
            //Block until
        }
    }

    private def harnessDone() {
        gettingHarnessed = false;
        when(doneDelivering) {
            //Block until done
        }
        doneDelivering = false;
    }
}
