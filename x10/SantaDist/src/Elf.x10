import x10.util.Random;

public class Elf {

    private val name:String;
    private val santa:GlobalRef[SantaClaus];
    private val random:Random;
    private var hasProblem:Boolean = false;
    private var refThis:GlobalRef[Elf];

    public def this(n:String, s:GlobalRef[SantaClaus]) {
        name = n + " @ " + here + ": ";
        santa = s;
        random = new Random();
    }

    public def run() {
        setRef();
        while(true) {
            work();
            waitForSanta();
        }
    }

    public def fixProblem() {
        atomic {
            hasProblem = false;
        }
    }

    private def setRef() {
        refThis = GlobalRef[Elf](this);
    }

    private def work() {
        while(!hasProblem) {
            System.sleep(random.nextLong(2500) + 500);
            if(random.nextLong(100) < 20) {
                hasProblem = true;
            }
        }
    }

    private def waitForSanta() {
        Console.OUT.println(name + "has problem and goes to shop door");
        at (santa.home) async {
            santa().addToElfQueue(refThis);
        }
        when(!hasProblem) {
            //Just block until fixed
        }
    }
}
