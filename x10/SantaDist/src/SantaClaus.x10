import x10.util.ArrayList;
import x10.util.Random;

public class SantaClaus {

    private val name:String;
    private val deerQueue:ArrayList[GlobalRef[Reindeer]] = new ArrayList[GlobalRef[Reindeer]](Main.REINDEER);
    private val elfQueue:ArrayList[GlobalRef[Elf]] = new ArrayList[GlobalRef[Elf]](Main.ELVES);
    private val random:Random;
    private var waiting:Long = 0;
    private var reindeerReady:Boolean = false;
    private var harnessDone:Long = 0;

    public def this(n:String) {
        name = n + " @ " + here + ": ";
        random = new Random();
    }

    public def run() {
        while(true) {
            //Block if none are waiting
            goToSleep();
            //Someone is waiting
            checkDoor();
        }
    }

    public def addToElfQueue(elfRef:GlobalRef[Elf]) {
        atomic {
            elfQueue.add(elfRef);
            if(elfQueue.size() % Main.ELF_GRP == 0) {
                waiting++;
            }
        }
    }

    public def addToDeerQueue(deerRef:GlobalRef[Reindeer]) {
        atomic {
            deerQueue.add(deerRef);
            if(deerQueue.size() == Main.REINDEER) {
                reindeerReady = true;
                waiting++;
            }
        }
    }

    public def harnessDone() {
        atomic {
            harnessDone++;
        }
    }

    private def goToSleep() {
        Console.OUT.println(name + "goes to sleep");
        when(waiting > 0) {
            //Block until someone is waiting
        }
    }

    private def getDeerReady() {
        finish {
            for(deer in deerQueue) {
                at (deer.home) async {
                    deer().harness();
                }
            }
        }
    }
    
    private def deliverGifts() {
        Console.OUT.println(name + "delivering gifts");
        System.sleep(random.nextLong(3000) + 2000);
        for(deer in deerQueue) {
            at (deer.home) async {
                deer().doneDelivering();
            }
        }
        atomic {
            deerQueue.clear();
            reindeerReady = false;
        }
    }

    private def checkDoor() {
        if(reindeerReady) {
            getDeerReady();
            deliverGifts();

        } else {
            helpElves();
        }
        atomic {
            waiting--;
        }
    }


    private def helpElves() {
        Console.OUT.println(name + "helping elves");
        for(elf in 1..Main.ELF_GRP) {
            at (elfQueue.getFirst().home) async {
                elfQueue.getFirst()().fixProblem();
            }
            elfQueue.removeFirst();
        }
    }

}
