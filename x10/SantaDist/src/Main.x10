
public class Main {

    public static val ELVES = 20;
    public static val ELF_GRP = 3;
    public static val REINDEER = 9;
    
    public static def main(Rail[String]) {
        val world = Place.places();
        val santa = new SantaClaus("Santa");
        val sRef = GlobalRef[SantaClaus](santa);
        finish {
            async {
                santa.run();
            }
            for(place in 1..9) {
                at (world(place)) async {
                    new Reindeer("Reindeer" + place, sRef).run();
                }
            }
            for(place in 10..30) {
                at (world(place)) async {
                    new Elf("Elf " + (place - 9), sRef).run();
                }
            }
        }
    }

}


