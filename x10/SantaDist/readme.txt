Santa Claus Problem readme:

Requirements:
    Command-line tools v.2.5.1 (http://sourceforge.net/projects/x10/files/x10/2.5.1/)
        Download the tools that fit your setup (this project was developed using x10-2.5.1_linux_x86_64.tgz)
    For managed: Java jdk or jre (this project was developed using java-7-openjdk)
    For native: C++ tools (this project was developed using G++ 4.8.2)

X10 configuration:
(These are env variables to be set before compiling and running the program)
(only for managed)JAVA_HOME=<set_to_java_home_dir> (example: export JAVA_HOME=/usr/lib/jvm/java-7-openjdk)

X10_NTHREADS=<set_to_number_of_cores> (example (on quad-core): export X10_NTHREADS=4)

X10_NPLACES=<processes_in_program> (example (santa=1, reindeer=9, elves=20): export X10_NPLACES=30)

X10 compilation:
(Generated files will always be put in the current directory.)

    Change directory to a location where you want your executables to be
    (Java) Call x10c on <name>.x10 (file containing the main (Main.x10))
        Run: Call x10 with classpath parameter pointing to the current directory (where the generated class files are)
                x10 -classpath . <name>.java (file containing the main (Main.java))

    (C++) Call x10c++ on <name>.x10 with output parameter for naming the executable
                x10c++ <name>.x10 -o <lower_case_name> (file containing the main (Main.x10))
        Run: Execute the executable ./<name> (name chosen at compilation (santamain))


X10 optimiziations:
-O flag for optimized compilation
-x10rt standalone for running on one host (one machine)
