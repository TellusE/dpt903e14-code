
public class SantaMain {

    public static val ELVES = 20;
    public static val ELF_GRP = 3;
    public static val REINDEER = 9;
    
    public static def main(Rail[String]) {
        val santa = new SantaClaus("Santa");
        async {
            santa.run();
        }
        for(i in 1..20) {
            async {
                new Elf("Elf " + i, santa).run();
            }
        }
    }

}


