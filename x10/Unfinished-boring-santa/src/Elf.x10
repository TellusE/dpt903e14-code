import x10.util.Random;

public class Elf {

    private val name:String;
    private val santa:SantaClaus;
    private var hasProblem:Boolean = false;
    private val random:Random;

    public def this(n:String, s:SantaClaus) {
        name = n;
        santa = s;
        random = new Random();
    }

    public def run() {
        while(true) {
            work();
            notifySanta();
            waitForFix();
        }
    }

    public def fixProblem() {
        atomic {
            hasProblem = false;
        }
    }

    private def work() {
        while(!hasProblem) {
            System.sleep(random.nextLong(2500) + 500);
            if(random.nextLong(100) < 20) {
                hasProblem = true;
            }
        }
    }

    private def notifySanta() {
        Console.OUT.println(name + ": has problem and notifies Santa");
        santa.addToElfQueue(this);
    }

    private def waitForFix() {
        when(!hasProblem) {
            //Just block until fixed
        }
    }

}
