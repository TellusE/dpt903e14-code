import x10.util.Random;

public class Reindeer {

    private val name:String;
    private val santa:SantaClaus;
    private val random:Random;
    private var gettingHarnessed:Boolean = false;
    private var doneDelivering:Boolean = false;

    public def this(n:String, s:SantaClaus) {
        name = n;
        santa = s;
        random = new Random();
    }

    public def run() {
        goOnVacation();
        waitInHut();
        harnessDone();
        
    }

    public def harness() {
        atomic {
            gettingHarnessed = true;
        }
    }

    public def doneDelivering() {
        atomic {
            doneDelivering = true;
        }
    }

    private def goOnVacation() {
        System.sleep(random.nextLong(20000) + 10000);
    }

    private def waitInHut() {
        Console.OUT.println(name + ": arrived at hut");
        santa.addToDeerQueue(this);
        when(gettingHarnessed) {
            //Block until
        }
    }

    private def harnessDone() {
        santa.harnessDone();
    }
}
