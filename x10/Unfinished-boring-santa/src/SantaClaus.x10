import x10.util.ArrayList;

public class SantaClaus {

    private val name:String;
    private val deerQueue:ArrayList[Reindeer] = new ArrayList[Reindeer](SantaMain.REINDEER);
    private val elfQueue:ArrayList[Elf] = new ArrayList[Elf](SantaMain.ELVES);
    private var waiting:Long = 0;
    private var reindeerReady:Boolean = false;
    private var harnessDone:Long = 0;

    public def this(n:String) {
        name = n;
    }

    public def run() {
        while(true) {
            //Block if none are waiting
            goToSleep();
            //Someone is waiting
            checkDoor();
        }
    }

    public def addToElfQueue(elf:Elf) {
        atomic {
            elfQueue.add(elf);
            if(elfQueue.size() % SantaMain.ELF_GRP == 0) {
                waiting++;
            }
        }
    }

    public def addToDeerQueue(deer:Reindeer) {
        atomic {
            deerQueue.add(deer);
            if(deerQueue.size() == SantaMain.REINDEER) {
                reindeerReady = true;
                waiting++;
            }
        }
    }

    public def harnessDone() {
        atomic {
            harnessDone++;
        }
    }

    private def goToSleep() {
        when(waiting >0) {
            //Block until
        }
    }

    private def checkDoor() {
        if(reindeerReady) {
            //Deliver gifts
        } else {
            helpElves();
        }
        atomic {
            waiting--;
        }
    }


    private def helpElves() {
        Console.OUT.println(name + ": helping elves");
        for(i in 1..3) {
            elfQueue.getFirst().fixProblem();
            elfQueue.removeFirst();
        }
    }

}
