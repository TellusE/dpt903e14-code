
public class Test {

    public static def main(Rail[String]) {

        val some1 = new Some("Some1");
        val href1 = GlobalRef[Some](some1);

        at (Place.places()(1)) {
            val some2 = new Some("Some2");
            val href2 = GlobalRef[Some](some2);
            at(href1.home) {
                href1().setRef(href2);
                href1().print();
            }
        }

        at (some1.ref.home) {
            some1.ref().setRef(href1);
            some1.ref().print();
        }

    }
}
