public class Some {
        
        var name:String;
        public var ref:GlobalRef[Some];

        public def this(n:String) {
            name = n;
        }

        public def change(n:String) {
            name = n;
        }

        public def setRef(g:GlobalRef[Some]) {
            ref = g;
        }

        public def print() {
            Console.OUT.println(name + " @ " + here);
        }
}

