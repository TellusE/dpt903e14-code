import x10.util.Random;
import x10.util.Timer;
import x10.util.RailBuilder;

public class Main {
    
    public static def main(argv:Rail[String]) {
        if (argv.size > 0) {
            val size:Long = Long.parse(argv(0));
            val r:Random = new Random();
            val builder:RailBuilder[Long] = new RailBuilder[Long]();
            val origin = new Rail[Int](size, (Long)=>r.nextInt(9999n));
            for (i in 1..10) {
                var data:Rail[Int] = new Rail[Int](origin);
                //Console.OUT.println("Rail sorted? " + Qutils.isSorted(data));
                var startTime:Long = Timer.nanoTime();
                //Quicksort.sort(data);
                ParQuicksort.sort(data);
                var endTime:Long = Timer.nanoTime();
                //Console.OUT.println("Rail sorted? " + Qutils.isSorted(data));
                builder.add((endTime - startTime));
            }

            val result = builder.result();
            for (i in result) {
                Console.OUT.print(i + ", ");
            }
            Console.OUT.println("");
            var res:Long = 0;
            for (i in result) {
                res += i;
            }
            Console.OUT.println("Average: " + (res / result.size));

        } else {
            Console.OUT.println("Expecting number of elements argument");
        }
    }
}
