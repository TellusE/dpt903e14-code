
public class ParQuicksort {

    public static def sort(toSort:Rail[Int]) {
        parSort(toSort, 0, toSort.size-1, (Runtime.NTHREADS * 2));
    }

    public static def parSort(toSort:Rail[Int],
        left:Long, right:Long, depth:Long) {
        var pivot:Long = Qutils.partition(toSort, left, right);
        if (depth > 0) {
            finish {
                if (left < pivot - 1) {
                    async parSort(toSort, left, pivot - 1, (depth - 1));
                }
                if (pivot < right) {
                    async parSort(toSort, pivot, right, (depth - 1));
                }
            }
        } else {
            if (left < pivot - 1) {
                Quicksort.qSort(toSort, left, pivot - 1);
            }
            if (pivot < right) {
                Quicksort.qSort(toSort, pivot, right);
            }
        }
    }
}
