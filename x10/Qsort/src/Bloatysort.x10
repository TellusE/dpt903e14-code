import x10.util.RailBuilder;

public class Bloatysort {

    public static def sort(toSort:Rail[Int]) {
        if (toSort.size < 2) return toSort;
        pivot:Int = toSort((toSort.size/2) as Long);
        var s1:Rail[Int];
        var s2:Rail[Int];
        var s3:Rail[Int];
        finish {
            async { s1 = sort(lessThan(toSort, pivot)); }
            async { s2 = eqTo(toSort, pivot); }
            s3 = sort(grThan(toSort, pivot));
        }
        val resRail = new RailBuilder[Int]();
        for (i in s1) {
            resRail.add(i);
        }
        for (i in s2) {
            resRail.add(i);
        }
        for (i in s3) {
            resRail.add(i);
        }
        return resRail.result();
    }

    public static def lessThan(srcRail:Rail[Int], target:Int) {
        val tmpRail = new RailBuilder[Int]();
        for (i in 0..(srcRail.size - 1)) {
            if (srcRail(i) < target) tmpRail.add(srcRail(i));
        }
        return tmpRail.result();
    }

    public static def eqTo(srcRail:Rail[Int], target:Int) {
        val tmpRail = new RailBuilder[Int]();
        for (i in 0..(srcRail.size - 1)) {
            if (srcRail(i) == target) tmpRail.add(srcRail(i));
        }
        return tmpRail.result();
    }

    public static def grThan(srcRail:Rail[Int], target:Int) {
        val tmpRail = new RailBuilder[Int]();
        for (i in 0..(srcRail.size - 1)) {
            if (srcRail(i) > target) tmpRail.add(srcRail(i));
        }
        return tmpRail.result();
    }
}

