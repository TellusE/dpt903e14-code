
public class Quicksort {

    public static def sort(toSort:Rail[Int]) {
        qSort(toSort, 0, toSort.size-1);
    }

    public static def qSort(toSort:Rail[Int], left:Long, right:Long) {
        var pivot:Long = Qutils.partition(toSort, left, right);
        if (left < pivot - 1) {
            qSort(toSort, left, pivot - 1);
        }
        if (pivot < right) {
            qSort(toSort, pivot, right);
        }
    }
}
