
public class Qutils {

    public static def printRail(rail:Rail[Int]) {
        for (i in 0..(rail.size - 1)) {
                Console.OUT.print(rail(i));
                Console.OUT.print(", ");
        }
            Console.OUT.println("");
    }

    public static def isSorted(rail:Rail[Int]) {
        if (rail.size > 1) {
            for (i in 0..(rail.size - 2)) {
                if (rail(i) > rail(i+1)) {
                    return false;
                }
            }
        }
        return true;
    }

    public static def partition(toSort:Rail[Int], left:Long, right:Long) {
        var ltmp:Long = left;
        var rtmp:Long = right;
        var tmp:Int;
        var pivot:Long = toSort((left + right) / 2);
        
        while (ltmp <= rtmp) {
            while (toSort(ltmp) < pivot) {
                ltmp++;
            }
            while (toSort(rtmp) > pivot) {
                rtmp--;
            }
            if (ltmp <= rtmp) {
                tmp = toSort(ltmp);
                toSort(ltmp) = toSort(rtmp);
                toSort(rtmp) = tmp;
                ltmp++;
                rtmp--;
            }
        }
        return ltmp;
    }
}

