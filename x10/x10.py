"""
    Pygments lexers for X10.
    :copyright: Copyright 2015 by the Pygments teahm, see AUTHORS.
    :license: BSD, see LICENSE for details.
    
    :The X10Lexer(RegexLexer) is a modified JavaLexer(RegexLexer)
        modified by:
            - Johannes Lindhart Borresen <jborre10@student.aau.dk>
            - Birgir Mar Eliasson <belias10@student.aau.dk>
"""

__all__ = ['X10Lexer']

import re

from pygments.lexer import Lexer, RegexLexer, include, bygroups, using, \
    this, combined, default, words
from pygments.token import Text, Comment, Operator, Keyword, Name, String, \
    Number, Punctuation

class X10Lexer(RegexLexer):
    """
    For X10 <http://x10-lang.org>_ source code
    """

    name = "X10"
    aliases = ['x10']
    filenames = ['*.x10']

    flags = re.MULTILINE | re.DOTALL

    tokens = {
            'root': [
                (r'[^\S\n]+', Text),
                (r'//.*?\n', Comment.Single),
                (r'/\*.*?\*/', Comment.Multiline),
                (r'(assert|break|case|catch|continue|default|do|else|finally|for|'
                    r'async|when|atomic|clocked|at|in|' #x10
                    r'if|goto|instanceof|new|return|switch|this|throw|try|while)\b',
                    Keyword),
                (r'((?:(?:[^\W\d]|\$)[\w.\[\]$<>]*\s+)+?)'  # return arguments
                    r'((?:[^\W\d]|\$)[\w$]*)'               # method name
                    r'(\s*)(\()',                           # signature start
                    bygroups(using(this), Name.Function, Text, Operator)),
                (r'@[^\W\d][\w.]*', Name.Decorator),
                (r'(abstract|extends|final|implements|native|private|val|var|def|'
                    r'protected|public|static|strictfp|super|synchronized|throws|'
                    r'transient|volatile)\b', Keyword.Declaration),
                (r'(void)\b', Keyword.Type),
                (r'(?<=:)(\w*\d*)', Keyword.Type),
                (r'(true|false|null|haszero)\b', Keyword.Constant),
                (r'(package)(\s+)', bygroups(Keyword.Namespace, Text), 'import'),
                (r'(true|false|null)\b', Keyword.Constant),
                (r'(class|interface)(\s+)', bygroups(Keyword.Declaration, Text), 'class'),
                (r'(import)(\s+)', bygroups(Keyword.Namespace, Text), 'import'),
                (r'"(\\\\|\\"|[^"])*"', String),
                (r"'\\.'|'[^\\]'|'\\u[0-9a-fA-F]{4}'", String.Char),
                (r'(\.)((?:[^\W\d]|\$)[\w$]*)', bygroups(Operator, Name.Attribute)),
                (r'^\s*([^\W\d]|\$)[\w$]*:', Name.Label),
                (r'([^\W\d]|\$)[\w$]*', Name),
                (r'[~^*!%&\[\](){}<>|+=:;,./?-]', Operator),
                (r'[0-9][0-9]*\.[0-9]+([eE][0-9]+)?[fd]?', Number.Float),
                (r'0x[0-9a-fA-F]+', Number.Hex),
                (r'[0-9]+(_+[0-9]+)*L?', Number.Integer),
                (r'\n', Text)
            ],
            'class': [
                (r'([^\W\d]|\$)[\w$]*', Name.Class, '#pop')
            ],
            'import': [
                (r'[\w.]+\*?', Name.Namespace, '#pop')
            ],
    }
