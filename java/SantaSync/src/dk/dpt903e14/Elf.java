package dk.dpt903e14;

import dk.dpt903e14.print.ConcurrentPrinter;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by nattfari on 12/13/14.
 */
public class Elf implements Runnable{

    private final String name;
    private final AtomicBoolean hasProblem;
    private final CyclicBarrier shopDoor;
    private final ArrayBlockingQueue<Elf> doorQueue;

    private final ConcurrentPrinter concurrentPrinter;

    public Elf(String objectName, CyclicBarrier door, ArrayBlockingQueue<Elf> queue) {
        this.name = objectName;
        this.hasProblem = new AtomicBoolean(false);
        this.shopDoor = door;
        this.doorQueue = queue;
        concurrentPrinter = ConcurrentPrinter.getInstance();
    }

    @Override
    public void run() {
        //concurrentPrinter.printMsg(this.name, "Spawned");
        while(!Thread.currentThread().isInterrupted()) {
            this.work();
            this.goWaitAtDoor();
            this.waitForProblemFixed();
        }
    }

    private synchronized void waitForProblemFixed() {
        while(this.hasProblem.get()) {
            try {
                this.wait();
            } catch (InterruptedException ex) {
                //Swallow it
            }
        }
    }

    private synchronized void goWaitAtDoor() {
        try {
            this.doorQueue.put(this);
            this.shopDoor.await();
        } catch (BrokenBarrierException|InterruptedException ex) {
            //Swallow it
        }
    }

    private void work() {
        //Work random periods of time and if problem, break out of loop
        while(!hasProblem.get()) {
            try {
                Thread.sleep(ThreadLocalRandom.current().nextLong(500, 2000));
            } catch (InterruptedException ex) {
                //Swallow it
            }
            if(ThreadLocalRandom.current().nextInt(0, 100) < 10) {
                //10% chance that a problem is encountered
                concurrentPrinter.printMsg(this.name, "Encountered a problem");
                this.hasProblem.set(true);
            }
        }
    }

    public synchronized void fixProblem() {
        this.hasProblem.set(false);
        this.notify();
    }

}
