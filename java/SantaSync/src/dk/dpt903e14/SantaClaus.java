package dk.dpt903e14;

import dk.dpt903e14.print.ConcurrentPrinter;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by nattfari on 12/13/14.
 */
public class SantaClaus implements Runnable{

    private final String name;
    private final AtomicBoolean reindeerReady;
    private final AtomicBoolean reindeerHarnessed;
    private final AtomicInteger waitingAtDoor;
    private final ArrayList<Reindeer> deerList;
    private final ArrayBlockingQueue<Elf> elfList;
    private final ConcurrentPrinter concurrentPrinter;

    public SantaClaus(String objectName, ArrayList<Reindeer> deerList, ArrayBlockingQueue<Elf> elfList) {
        this.name = objectName;
        this.concurrentPrinter = ConcurrentPrinter.getInstance();
        this.waitingAtDoor = new AtomicInteger(0);
        this.reindeerReady = new AtomicBoolean(false);
        this.reindeerHarnessed = new AtomicBoolean(false);
        this.deerList = deerList;
        this.elfList = elfList;
    }

    @Override
    public void run() {
        //concurrentPrinter.printMsg(this.name, "Spawned");
        while(!Thread.currentThread().isInterrupted()) {
            this.goToSleep();
            this.checkDoor();
        }
    }

    private synchronized void goToSleep() {
        concurrentPrinter.printMsg(this.name, "Going to sleep");
        while(waitingAtDoor.get() == 0) {
            try {
                //Block until someone is at the door (reindeer or elf)
                this.wait();
            } catch (InterruptedException ex) {
                //Swallow it, the isInterrupted() status should
                //be set and the thread should quit running
            }
        }
        this.waitingAtDoor.decrementAndGet();
        concurrentPrinter.printMsg(this.name, "Woken up");
    }

    private void checkDoor() {
        if(this.reindeerReady.get()) {
            this.reindeerReady.set(false);
            //Reindeer are here, get them ready for a sleigh ride
            this.harnessReindeer();
            this.deliverGifts();
        } else {
            //Since the reindeer are not ready, there is a group of elves waiting
            this.helpElves();
        }
    }

    private void helpElves() {
        for(int i = 0; i < Main.ELF_GRP; i++) {
            //Santa takes the first three(ELF_GRP) elves and fixes their problems
            try {
                this.elfList.take().fixProblem();
            } catch (InterruptedException e) {
                //Swallow it
            }
        }
        this.concurrentPrinter.printMsg(this.name, "Done helping elves");
    }

    private synchronized void harnessReindeer() {
        //Iterate through deerList and call harness on them
        for(Reindeer deer : deerList) {
            deer.harness();
        }
        while(!this.reindeerHarnessed.get()) {
            try {
                //Wait until all deer are properly harnessed before delivering gifts
                this.wait();
            } catch (InterruptedException ex) {
                //Swallow it
            }
        }
    }

    private void deliverGifts() {
        concurrentPrinter.printMsg(this.name, "Delivering gifts");
        try {
            Thread.sleep(ThreadLocalRandom.current().nextLong(2000, 5000));
        } catch (InterruptedException ex) {
            //Swallow it
        }
        this.reindeerHarnessed.set(false);
        concurrentPrinter.printMsg(this.name, "Done delivering");
        for (Reindeer deer : deerList) {
            deer.doneDelivering();
        }
    }

    public void signalReindeerReady() {
        this.reindeerReady.set(true);
    }

    public synchronized void signalReindeerHarnessed() {
        this.reindeerHarnessed.set(true);
        this.notify();
    }

    public synchronized void wakeUp() {
        this.waitingAtDoor.incrementAndGet();
        this.notify();
    }

}
