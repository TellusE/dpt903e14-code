package dk.dpt903e14;

import dk.dpt903e14.print.ConcurrentPrinter;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;

public class Main {

    public final static int REINDEER = 9;
    public final static int ELVES = 20;
    public final static int ELF_GRP = 3;

    public static void main(String[] args) {
        final ArrayList<Reindeer> reindeerList = new ArrayList<>(REINDEER);
        //Elf queue is FIFO (removal and blocking)
        //Fairness parameter set: 'true'
        final ArrayBlockingQueue<Elf> elfQueue = new ArrayBlockingQueue<>(ELVES, true);

        //Concurrent printer that prints from msg queue
        final ExecutorService printSpawner = Executors.newSingleThreadExecutor();
        final ConcurrentPrinter concurrentPrinter = ConcurrentPrinter.getInstance();
        printSpawner.submit(concurrentPrinter);

        //SantaClaus thread
        final ExecutorService santaSpawner = Executors.newSingleThreadExecutor();
        final SantaClaus santaClaus = new SantaClaus("Santa", reindeerList, elfQueue);
        santaSpawner.submit(santaClaus);

        //Elf barrier to synchronize only on a group of three. No more, no less
        final CyclicBarrier shopDoor = new CyclicBarrier(ELF_GRP, new Runnable() {
            @Override
            public void run() {
                //Notify santa of elves at door
                santaClaus.wakeUp();
            }
        });

        //Elf threads
        final ExecutorService elfSpawner = Executors.newFixedThreadPool(ELVES);
        for (int elf = 1; elf <= ELVES; elf++) {
            elfSpawner.submit(new Elf("Elf" + elf, shopDoor, elfQueue));
        }

        //Reindeer barrier to notify of their return
        final CyclicBarrier reindeerHut = new CyclicBarrier(REINDEER, new Runnable() {
            @Override
            public void run() {
                //Notify santa of reindeer in hut
                santaClaus.signalReindeerReady();
                santaClaus.wakeUp();
            }
        });

        //Reindeer barrier to signal successful harnessing
        final CyclicBarrier reindeerHarnessed = new CyclicBarrier(REINDEER, new Runnable() {
            @Override
            public void run() {
                //Notify santa that reindeer are ready to deliver presents
                santaClaus.signalReindeerHarnessed();
            }
        });

        //Reindeer threads
        final ExecutorService reindeerSpawner = Executors.newFixedThreadPool(REINDEER);
        for (int deer = 1; deer <= REINDEER; deer++) {
            Reindeer someDeer = new Reindeer("Reindeer" + deer, reindeerHut, reindeerHarnessed);
            reindeerList.add(someDeer);
            reindeerSpawner.submit(someDeer);
        }
    }
}
