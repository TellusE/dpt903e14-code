package dk.dpt903e14;

import dk.dpt903e14.print.ConcurrentPrinter;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by nattfari on 12/13/14.
 */
public class Reindeer implements Runnable{

    private final String name;
    private final CyclicBarrier reindeerHut;
    private final CyclicBarrier harnessedDone;
    private final AtomicBoolean gettingHarnessed;
    private final AtomicBoolean doneDelivering;

    private final ConcurrentPrinter concurrentPrinter;

    public Reindeer(String objectName, CyclicBarrier hut, CyclicBarrier harnessed) {
        this.name = objectName;
        this.reindeerHut = hut;
        this.harnessedDone = harnessed;
        this.gettingHarnessed = new AtomicBoolean(false);
        this.doneDelivering = new AtomicBoolean(false);
        concurrentPrinter = ConcurrentPrinter.getInstance();
    }

    @Override
    public void run() {
        //concurrentPrinter.printMsg(this.name, "Spawned");
        while(!Thread.currentThread().isInterrupted()) {
            this.goOnVacation();
            this.waitInHut();
            this.waitForHarness();
            this.harnessDone();
            this.waitForDeliveryDone();
        }
    }

    private void waitInHut() {
        concurrentPrinter.printMsg(this.name, "Arrived at hut");
        try {
            reindeerHut.await();
        } catch (BrokenBarrierException|InterruptedException ex) {
            //Swallow this (BrokenBarrier should not happen unless we're trying to abort execution)
        }
    }

    private void harnessDone() {
        this.gettingHarnessed.set(false);
        //concurrentPrinter.printMsg(this.name, "Harnessed");
        try {
            harnessedDone.await();
        } catch (BrokenBarrierException|InterruptedException ex) {
            //Swallow this
        }
    }

    private synchronized void waitForDeliveryDone() {
        while(!this.doneDelivering.get()) {
            try {
                //Block until santa is done delivering
                this.wait();
            } catch (InterruptedException ex) {
                //Swallow it
            }
        }
        this.doneDelivering.set(false);
    }

    private void goOnVacation() {
        //concurrentPrinter.printMsg(this.name, "Going on vacation");
        try {
            Thread.sleep(ThreadLocalRandom.current().nextLong(10000, 30000));
        } catch (IllegalArgumentException|InterruptedException ex) {
            //Swallow (Illegal will not happen since random only generates positive numbers)
        }

    }

    private synchronized void waitForHarness() {
        while(!this.gettingHarnessed.get()) {
            try {
                //Block until Santa has harnessed
                this.wait();
            } catch (InterruptedException ex) {
                //Swallow it
            }
        }
    }

    public synchronized void harness() {
        this.gettingHarnessed.set(true);
        this.notify();
    }

    public synchronized void doneDelivering() {
        this.doneDelivering.set(true);
        this.notify();
    }
}
