package dk.dpt903e14.print;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by nattfari on 12/16/14.
 */
public final class ConcurrentPrinter implements Runnable{

    private final ArrayBlockingQueue<String> msgQueue = new ArrayBlockingQueue<>(100000);

    private static ConcurrentPrinter ourInstance = new ConcurrentPrinter();

    public static ConcurrentPrinter getInstance() {
        return ourInstance;
    }

    private ConcurrentPrinter() {
    }

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println(msgQueue.take());
            } catch (InterruptedException ex) {
                //Swallow it
                ex.printStackTrace();
            }
        }
    }

    public void printMsg(String name, String msg) {
        try {
            msgQueue.put(name + ": " + msg);
        } catch (InterruptedException|ClassCastException|NullPointerException|IllegalArgumentException ex) {
            //Swallow it since only interruptedEx should be thrown
            ex.printStackTrace();
        }
    }
}
