package Quicksort;

import java.util.Vector;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

public class ParQuicksort {

    private static final ExecutorService executors = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private static Random random = new Random();

    private static AtomicInteger availableThreads = new AtomicInteger(Runtime.getRuntime().availableProcessors());
    
    static class Sorter implements Callable<Vector<Integer>> {
        protected Vector<Integer> toSort;

        public Sorter(Vector<Integer> toSort) {
            this.toSort = toSort;
        }
        
        public Vector<Integer> call() {
            if (toSort.size() <= 1) return toSort;

            Integer pivot = toSort.get(random.nextInt(toSort.size()));
        
            Vector<Integer> left = new Vector<>(),
                right = new Vector<>(),
                middle = new Vector<>();

            for (Integer i : toSort) {
                if (i < pivot) left.add(i);
                else if (i == pivot) middle.add(i);
                else right.add(i);
            }

            if (availableThreads.get() > 0) {
                availableThreads.addAndGet(-2);
                
                Future<Vector<Integer>> leftFuture = executors.submit(new Sorter(left));
                Future<Vector<Integer>> rightFuture = executors.submit(new Sorter(right));
                
                try {
                    left = leftFuture.get();
                    left.addAll(middle);
                    left.addAll(rightFuture.get());
                } catch (Exception e) {
                    System.out.println("Bad future juju!");
                }

                availableThreads.addAndGet(2);
            } else {
                left = new Sorter(left).call();
                right = new Sorter(right).call();

                left.addAll(middle);
                left.addAll(right);
            }
                
            return left;
        }
    }
   
    public static Vector<Integer> sort(Vector<Integer> toSort) {
        ParQuicksort.Sorter s = new ParQuicksort.Sorter(toSort);
        Vector<Integer> returnValue = s.call();
        executors.shutdown(); // Necessary to avoid hangs.
        return returnValue; 
    }
};
