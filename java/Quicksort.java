package Quicksort;

import java.util.Vector;
import java.util.Random;

public class Quicksort {    
    protected Vector<Integer> toSort;

    private Boolean isSorted = false;
    
    public Quicksort(Vector<Integer> doSort) {
        toSort = doSort;
    }

    public Vector<Integer> sort() {
        if (toSort.size() <= 1) {
            isSorted = true;
            return toSort;
        }

        Random r = new Random();
        
        Integer pivot = toSort.get(r.nextInt(toSort.size()));
        
        Vector<Integer> left = new Vector<>(),
            right = new Vector<>(),
            middle = new Vector<>();

        for (Integer i : toSort) {
            if (i < pivot) left.add(i);
            else if (i == pivot) middle.add(i);
            else right.add(i);
        }

        left = new Quicksort(left).sort();
        right = new Quicksort(right).sort();
        
        left.addAll(middle);
        left.addAll(right);

        isSorted = true;

        toSort = left;
        
        return left;
    }

    public Vector<Integer> get() {
        return isSorted ? toSort : sort();
    }
};
