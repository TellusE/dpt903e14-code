import java.util.ArrayList;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hi there, man!");
        if (args.length > 0) {
            ArrayList<Integer> toSort = new ArrayList<>();
            for (String s : args) {
                toSort.add(Integer.parseInt(s));
            }
            System.out.println(Quicksort.doSort(toSort));
        }
    }
}
