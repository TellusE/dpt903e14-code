package com.company;

/**
 * Created by nattfari on 1/11/15.
 */
public class Quicksort {

    public static void sort(final int toSort[]) {
        qSort(toSort, 0, toSort.length - 1);
    }

    public static void qSort(final int toSort[],final int left,final int right) {
        final int pivot = Qutils.partition(toSort, left, right);
        if (left < pivot - 1) {
            qSort(toSort, left, pivot - 1);
        }
        if (pivot < right) {
            qSort(toSort, pivot, right);
        }

    }
}
