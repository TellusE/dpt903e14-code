package com.company;

import java.util.concurrent.*;

/**
 * Created by nattfari on 1/11/15.
 */
public class ParQuicksort {
    private static final ThreadPoolExecutor executor =
            (ThreadPoolExecutor)Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);

    public static void sort(final int array[]) {
        parSort(array, 0, array.length - 1);
    }

    public static void parSort(
            final int array[], final int left, final int right) {
        final int pivot = Qutils.partition(array, left, right);
        if (executor.getActiveCount() < executor.getMaximumPoolSize()) {
            Future<Void> future1 = null;
            Future<Void> future2 = null;
            if (left < pivot - 1) {
                future1 = executor.submit(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        parSort(array, left, pivot - 1);
                        return null;
                    }
                });
            }
            if (pivot < right) {
                future2 = executor.submit(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        parSort(array, pivot, right);
                        return null;
                    }
                });
            }
            try {
                future1.get();
                future2.get();
            } catch (InterruptedException|ExecutionException ex) {}
        } else {
            if (left < pivot - 1) {
                Quicksort.qSort(array, left, pivot - 1);
            }
            if (pivot < right) {
                Quicksort.qSort(array, pivot, right);
            }
        }
    }
}
