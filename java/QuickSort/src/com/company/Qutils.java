package com.company;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

/**
 * Created by nattfari on 1/11/15.
 */
public class Qutils {

    public static long avg(ArrayList<Long> arrayList) {
        long sum = 0;
        Iterator<Long> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            sum += iterator.next();
        }
        return sum/arrayList.size();
    }

    public static void printArray(int array[]) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(String.valueOf(array[i]) + ", ");
        }
        System.out.println();
    }

    public static boolean isSorted(int array[]) {
        if (array.length > 1) {
            for (int i = 0; i < array.length-1; i++) {
                if (array[i] > array[i+1]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static int partition(int toSort[], int left, int right) {
        int ltmp = left;
        int rtmp = right;
        int tmp;
        int pivot = toSort[((int)(left + right) / 2)];
        while (ltmp <= rtmp) {
            while (toSort[ltmp] < pivot) {
                ltmp++;
            }
            while (toSort[rtmp] > pivot) {
                rtmp--;
            }
            if (ltmp <= rtmp) {
                tmp = toSort[ltmp];
                toSort[ltmp] = toSort[rtmp];
                toSort[rtmp] = tmp;
                ltmp++;
                rtmp--;
            }
        }
        return ltmp;
    }

}
