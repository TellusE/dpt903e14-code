package com.company;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {
    public static void main(String[] args) {
	// write your code here
        if (args.length > 0) {
            int size = Integer.parseInt(args[0]);
            Random rand = new Random();
            int[] origin = new int[size];
            for (int i = 0; i < origin.length; i++) {
                origin[i] = rand.nextInt(9999);
            }
            ArrayList<Long> timestamps = new ArrayList<>();
            for(int i = 0; i < 10; i++) {
                int[] array = new int[size];
                System.arraycopy(origin, 0, array, 0, size);
                System.out.println("Array sorted? " + String.valueOf(Qutils.isSorted(array)));
                long startTime = System.nanoTime();
                ParQuicksort.sort(array);
                long endtime = System.nanoTime();
                System.out.println("Array sorted? " + String.valueOf(Qutils.isSorted(array)));
                timestamps.add(endtime - startTime);
            }
            Iterator<Long> iterator = timestamps.iterator();
            while (iterator.hasNext()) {
                System.out.print(String.valueOf(iterator.next()) + ", ");
            }
            System.out.println();
            System.out.println("Avg time: " + String.valueOf(Qutils.avg(timestamps)));
        }
    }
}
