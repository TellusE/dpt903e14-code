import java.util.concurrent.*;
import java.io.Console;

// Primary Santa Claus class.
public class SantaClaus {
    static final int REINDEER = 9,
        ELVES = 3;
    
    static Semaphore onlyElves = new Semaphore(3),
        eMutex = new Semaphore(1),
        rMutex = new Semaphore(1),
        reinWait = new Semaphore(0),
        sleigh = new Semaphore(0),
        done = new Semaphore(0),
        santaSignal = new Semaphore(0),
        santa = new Semaphore(0),
        problem = new Semaphore(0),
        elfDone = new Semaphore(0);

    static SantaClausRunnable rRunner = new Reindeer(),
        eRunner = new Elves(),
        sRunner = new Santa();

    static int reinCt = 0,
        elfCt = 0;
    
    private static abstract class SantaClausRunnable implements Runnable {
        private Boolean _isDone = false;
        private Boolean _activeCycle = false;

        protected String name = "NAMELESS";
        
        public Boolean isDone() { return _isDone; }
        
        public void cancel() {
            print("%s: cancelling", name);
            _isDone = true;
            while (_activeCycle) {
                // Wait for current cycle to conclude.
            }
            print("%s: cancelled", name);
        }

        public abstract void runMethod() throws InterruptedException;

        public void run() {
            print("%s: starting", name);
            try {
                while (!isDone()) {
                    _activeCycle = true;
                    runMethod();
                    _activeCycle = false;
                }
            } catch (InterruptedException e) {
                print("%s: InterruptedException thrown!", e.getMessage());
            }
        }

        protected void print(String msg, Object... args) {
            System.out.println(String.format(msg, args));
        }
    }
    
    private static class Reindeer extends SantaClausRunnable {
        public Reindeer() {
            name = "Reindeer";
        }
        
        public void runMethod() throws InterruptedException {
            print("%s (cancel? %s): cycle!", name, isDone());
            rMutex.acquire();
            reinCt++;
            if (reinCt == REINDEER) {
                rMutex.release();
                santa.release();
            } else {
                rMutex.acquire();
                reinWait.release();
            }
        }
    }

    private static class Elves extends SantaClausRunnable {
        public Elves() {
            name = "Elves";
        }

        public void runMethod() throws InterruptedException {
            onlyElves.acquire();
            eMutex.acquire();
            elfCt++;
            if (elfCt == ELVES) {
                eMutex.release();
                santa.release();
            } else {
                eMutex.acquire();
                santaSignal.release();
            }
            problem.acquire();
            elfDone.acquire();
            onlyElves.release();
        }
    }

    private static class Santa extends SantaClausRunnable {
        public Santa() {
            name = "Santa";
        }
        
        public void runMethod() throws InterruptedException {
            santa.acquire();
            if (reinCt == REINDEER) {
                reinCt = 0;
                for (int i = 0; i < REINDEER - 1; i++)
                    reinWait.release();
                for (int i = 0; i < REINDEER; i++)
                    sleigh.release();
                for (int i = 0; i < REINDEER; i++)
                    done.release();
            } else {
                for (int i = 0; i < ELVES - 1; i++)
                    santaSignal.release();
                eMutex.acquire();
                elfCt = 0;
                eMutex.release();
                for (int i = 0; i < ELVES; i++) {
                    problem.release();
                    elfDone.release();
                }
            }
        }
    }
    
    public static void main(String[] args)
    throws InterruptedException {
        System.out.println("Java port of Santa Claus reference implementation.");

        Thread rThread = new Thread(rRunner),
            sThread = new Thread(sRunner),
            eThread = new Thread(eRunner);

        System.out.println("Starting threads...");
        
        rThread.start();
        sThread.start();
        eThread.start();

        System.out.println("Threads started!");
        
        boolean done = false;

        String cmd;
        Console console = System.console();

        // Just for graceful output :D
        Thread.sleep(100);
        
        while (!done) {
            cmd = console.readLine("CMD|> ");

            switch(cmd) {
            case "help": {
                System.out.println("Known commands: quit");
                break;
            }
            case "quit": {
                System.out.println("Cancelling threads.");
                done = true;
                rRunner.cancel();
                eRunner.cancel();
                sRunner.cancel();
                break;
            }
            default: {
                System.out.println(String.format("Unknown command '%s', try 'help'.", cmd));
                break;
            }
            }
        }

        System.out.println("Bye!");
    }
}
