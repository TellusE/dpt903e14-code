package Quicksort;

import java.util.Random;
import java.util.Vector;

public class QuicksortMain {

    static Random random = new Random();
    
    public static void println(String msg, Object ... args) {
        System.out.println(String.format(msg, args));
    }

    public static void println(Object msg) {
        System.out.println(msg);
    }

    public static void print(String msg, Object ... args) {
        System.out.print(String.format(msg, args));
    }

    public static void print(Object msg) {
        System.out.print(msg);
    }

    public static Vector<Integer> randomVector(Integer size, int max, long seed) {
        Random r = new Random(seed);
        Vector<Integer> v = new Vector<>(size);

        for (int i = 0; i < size; i++) v.add(r.nextInt(max));

        return v;
    }

    public static Vector<Integer> randomVector(Integer size, int max) {
        Random r = new Random();
        Vector<Integer> v = new Vector<>(size);

        for (int i = 0; i < size; i++) v.add(r.nextInt(max));

        return v;
    }
    
    public static Vector<Integer> randomVector(Integer size) {
        return randomVector(size, 1000);
    }

    public static Vector<Integer> randomVector(Integer size, long seed) {
        return randomVector(size, 1000, seed);
    }
    
    // Parallel sorts.
    public static Vector<Integer> parallelSort() {
        return parallelSort(50000);
    }

    public static Vector<Integer> parallelSort(int size) {
        return parallelSort(randomVector(size));
    }

    public static Vector<Integer> parallelSort(int size, long seed) {
        return parallelSort(randomVector(size, seed));
    }

    public static Vector<Integer> parallelSort(Vector<Integer> toSort) {
        Vector<Integer> toReturn;
        
        // Parallel
        long start_time = System.nanoTime();
        toReturn = ParQuicksort.sort(toSort);
        long end_time = System.nanoTime();
        double diff = (end_time-start_time)/1e6;
        println("Parallel sort on %s elements took %sms.", toSort.size(), diff);
        if (!isSorted(toReturn)) println("ERROR! Not sorted!");

        return toReturn;
    }

    // Sequential sorts.
    public static Vector<Integer> sequentialSort() {
        return sequentialSort(50000);
    }
    
    public static Vector<Integer> sequentialSort(int size) {
        return sequentialSort(randomVector(size));
    }

    public static Vector<Integer> sequentialSort(int size, long seed) {
        return sequentialSort(randomVector(size, seed));
    }
    
    public static Vector<Integer> sequentialSort(Vector<Integer> toSort) {
        Quicksort qs = new Quicksort(toSort);

        Vector<Integer> r;
        
        // Sequential.
        long start_time = System.nanoTime();
        r = qs.sort();
        long end_time = System.nanoTime();
        double diff = (end_time-start_time)/1e6;
        println("Sequential sort on %s elements took %sms.", toSort.size(), diff);
        if (!isSorted(qs.get())) println("ERROR! Not sorted!");
        return qs.get();
    }
    
    public static boolean isSorted(Vector<Integer> toCheck) {
        for (int i = 0; i < toCheck.size()-1; i++) {
            if (toCheck.get(i) > toCheck.get(i+1)) return false;
        }
        return true;
    }
    
    public static void main(String[] args) {
        Vector<Integer> toSort, toSortPar;

        if (args.length == 0) {
            println("Usage:");
            println("PARallel|SEQuential <num> for specific random set.");
            println("<num> for full set (may be cached).");
            println("<num> <num2>...[numN] for specific sort.");
        } else if (args.length > 2 && args[0].toLowerCase().startsWith("seq")) {
            int count = Integer.parseInt(args[1]);
            if (args.length == 3) {
                long seed = Long.parseLong(args[2]);
                println("Sequential random test (seed: %s)", seed);
                sequentialSort(count, seed);
            } else {
                println("Sequential random test.");
                sequentialSort(count);
            }
        } else if (args.length > 2 && args[0].toLowerCase().startsWith("par")) {
            int count = Integer.parseInt(args[1]);
            if (args.length == 3) {
                long seed = Long.parseLong(args[2]);
                println("Parallel random test (seed: %s)", seed);
                parallelSort(count, seed);
            } else {
                println("Parallel random test.");
                parallelSort(count);
            }
        } else if (args.length == 1) {
            // Prepare a random vector.
            toSort = new Vector<>(Integer.parseInt(args[0]));
            toSortPar = new Vector<>(Integer.parseInt(args[0]));
            
            for (int i = 0, toAdd = random.nextInt(1001);
                 i < toSort.capacity();
                 i++, toAdd = random.nextInt(1001)) {
                toSort.add(toAdd);
                toSortPar.add(toAdd);
            }

            // Make a deep copy for secondary vector.
            toSortPar = new Vector<>(toSort.size());
            for (Integer i : toSort) toSortPar.add(i);

            // Assertion.
            if (!toSortPar.equals(toSort)) println("Hm, the two vectors don't match!");

            parallelSort(toSortPar);
            sequentialSort(toSort);
        } else {
            print(String.format("Sorting numbers:"));
            for (String a : args) {
                print(String.format(" %s, ", a));
            }

            toSort = new Vector<>();
            toSortPar = new Vector<>();

            for (String s : args) {
                toSort.add(Integer.parseInt(s));
                toSortPar.add(Integer.parseInt(s));
            }

            
        }
        
        println("Bye!");
    }
}
